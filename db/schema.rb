# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170304202545) do

  create_table "assistants", force: :cascade do |t|
    t.string   "name"
    t.integer  "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "assistants", ["order_id"], name: "index_assistants_on_order_id"

  create_table "cars", force: :cascade do |t|
    t.string   "model"
    t.integer  "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "cars", ["order_id"], name: "index_cars_on_order_id"

  create_table "cemeteries", force: :cascade do |t|
    t.string   "name"
    t.string   "city"
    t.string   "province"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "crematoria", force: :cascade do |t|
    t.string   "name"
    t.string   "city"
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
    t.string   "A",          default: "CONTARINA SPA GESTIONE CREMATORI"
    t.string   "Tel",        default: "0422/212791-2 FAX 0422/212796"
    t.date     "Data"
  end

  create_table "deceaseds", force: :cascade do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.date     "birthday"
    t.date     "deathday"
    t.string   "born_place"
    t.string   "registered_in"
    t.string   "registered_in_address"
    t.string   "death_place"
    t.integer  "corpse_kind"
    t.date     "funeral_day"
    t.time     "funeral_time"
    t.string   "funeral_place"
    t.string   "cemetery_name"
    t.string   "coffin_kind"
    t.integer  "crematorium_kind"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.string   "coffin_prepare_by"
    t.string   "coffin_issued_by"
    t.string   "note"
    t.date     "flowerday"
    t.time     "flowertime"
    t.boolean  "pillow_take"
    t.boolean  "instruments_1",                default: false
    t.boolean  "instruments_2",                default: false
    t.boolean  "instruments_3",                default: false
    t.boolean  "instruments_4",                default: false
    t.boolean  "information_1",                default: false
    t.boolean  "information_2",                default: false
    t.date     "arrive_day"
    t.time     "arrive_time"
    t.string   "arrive_from"
    t.integer  "vase_issued_by"
    t.string   "invoice_company"
    t.string   "invoice_place"
    t.string   "invoice_street"
    t.string   "invoice_house"
    t.date     "exposure_day"
    t.time     "morgue_work_from"
    t.time     "morgue_work_to"
    t.date     "departure_day"
    t.time     "departure_time"
    t.integer  "marital_status"
    t.string   "profession"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.string   "applicant"
    t.string   "fax_mail"
    t.string   "lastname_declar"
    t.string   "fistname_declar"
    t.string   "born_place_declar"
    t.date     "birthday_declar"
    t.string   "registered_in_declar"
    t.string   "registered_in_address_declar"
    t.string   "rosary"
    t.date     "rosary_day"
    t.time     "rosary_time"
    t.string   "cemetery"
    t.string   "oven"
    t.string   "reserv_of"
    t.date     "burial_date"
    t.string   "death_case"
    t.integer  "commis_num"
    t.string   "office"
    t.date     "data"
    t.time     "deathtime"
  end

  create_table "documents", force: :cascade do |t|
    t.string   "document_type"
    t.string   "document_number"
    t.string   "document_issued_by"
    t.date     "document_when_issued"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "code_fiscale"
    t.string   "issued_by_municipality"
  end

  create_table "epigraphs", force: :cascade do |t|
    t.integer  "model"
    t.string   "title"
    t.string   "dedicated_to"
    t.string   "relatives"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "city_title"
    t.date     "date_vigil"
    t.time     "time_vigil"
  end

  create_table "flowers", force: :cascade do |t|
    t.string  "kind"
    t.integer "price"
    t.string  "text"
    t.integer "order_id"
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "deceased_id"
    t.integer  "relative_id"
    t.integer  "document_id"
    t.integer  "cemetery_id"
    t.integer  "crematorium_id"
    t.integer  "status",                    default: 0
    t.boolean  "operation_1",               default: false
    t.boolean  "operation_2",               default: false
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.string   "flower_note"
    t.string   "fiscal_code",    limit: 16
    t.integer  "epigraph_id"
    t.string   "flower_info"
    t.boolean  "operation_3",               default: false
    t.boolean  "operation_4",               default: false
    t.boolean  "operation_5",               default: false
    t.boolean  "operation_6",               default: false
    t.boolean  "operation_7",               default: false
    t.string   "note"
    t.string   "service_note"
  end

  create_table "relatives", force: :cascade do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.string   "relationship"
    t.string   "phone"
    t.string   "mobile"
    t.boolean  "mark",         default: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "mail"
    t.string   "resident"
    t.string   "via"
    t.string   "fiscal_code"
  end

end
