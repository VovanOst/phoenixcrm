class AddServiceNoteToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :service_note, :string
  end
end
