class AddDetailsToRelatives < ActiveRecord::Migration
  def change
    add_column :relatives, :mail, :string
    add_column :relatives, :resident, :string
    add_column :relatives, :via, :string
  end
end
