class AddElectroDeclarToDeceaseds < ActiveRecord::Migration
  def change
    add_column :deceaseds, :lastname_declar, :string
    add_column :deceaseds, :fistname_declar, :string
    add_column :deceaseds, :born_place_declar, :string
    add_column :deceaseds, :birthday_declar, :date
    add_column :deceaseds, :registered_in_declar, :string
    add_column :deceaseds, :registered_in_address_declar, :string
  end
end
