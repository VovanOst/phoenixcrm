class CreateCrematoria < ActiveRecord::Migration
  def change
    create_table :crematoria do |t|
      t.string :name
      t.string :city

      t.timestamps null: false
    end
  end
end
