class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :deceased
      t.references :relative
      t.references :document
      t.references :cemetery
      t.references :crematorium

      t.integer :status, default: 0
      t.boolean :operation_1, default: false
      t.boolean :operation_2, default: false
      t.timestamps null: false
    end
  end
end
