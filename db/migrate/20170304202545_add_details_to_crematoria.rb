class AddDetailsToCrematoria < ActiveRecord::Migration
  def change
    add_column :crematoria, :A, :string,  :default => "CONTARINA SPA GESTIONE CREMATORI"
    add_column :crematoria, :Tel, :string, :default => "0422/212791-2 FAX 0422/212796"
    add_column :crematoria, :Data, :date
  end
end
