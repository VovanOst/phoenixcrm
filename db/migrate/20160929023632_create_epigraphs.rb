class CreateEpigraphs < ActiveRecord::Migration
  def change
    create_table :epigraphs do |t|

      t.integer :model
      t.string :title
      t.string :dedicated_to
      t.string :relatives


      t.timestamps null: false
    end
    add_reference :orders, :epigraph
  end
end
