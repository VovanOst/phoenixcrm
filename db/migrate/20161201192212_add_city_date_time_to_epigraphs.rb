class AddCityDateTimeToEpigraphs < ActiveRecord::Migration
  def change
    add_column :epigraphs, :city_title, :string
    add_column :epigraphs, :date_vigil, :date
    add_column :epigraphs, :time_vigil, :time
  end
end
