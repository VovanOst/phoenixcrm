class AddDeathCaseToDeceaseds < ActiveRecord::Migration
  def change
    add_column :deceaseds, :death_case, :string
  end
end
