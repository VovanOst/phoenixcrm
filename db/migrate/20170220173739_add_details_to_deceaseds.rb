class AddDetailsToDeceaseds < ActiveRecord::Migration
  def change
    add_column :deceaseds, :rosary, :string
    add_column :deceaseds, :rosary_day, :date
    add_column :deceaseds, :rosary_time, :time
    add_column :deceaseds, :cemetery, :string
    add_column :deceaseds, :oven, :string
    add_column :deceaseds, :reserv_of, :string
    add_column :deceaseds, :burial_date, :date
  end
end
