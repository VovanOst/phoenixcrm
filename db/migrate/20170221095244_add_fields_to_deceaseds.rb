class AddFieldsToDeceaseds < ActiveRecord::Migration
  def change
    add_column :deceaseds, :commis_num, :integer
    add_column :deceaseds, :office, :string
    add_column :deceaseds, :data, :date
    add_column :deceaseds, :deathtime, :time
  end
end
