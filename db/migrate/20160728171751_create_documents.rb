class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|

      t.string :document_type
      t.integer :document_number
      t.string :document_issued_by
      t.date :document_when_issued

      t.timestamps null: false
    end
  end
end
