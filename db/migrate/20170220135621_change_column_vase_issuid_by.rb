class ChangeColumnVaseIssuidBy < ActiveRecord::Migration
  def change
    change_column :deceaseds, :vase_issued_by,  :string
  end
end
