class MoveFiscalCodeToOrder < ActiveRecord::Migration
  def change
    remove_column :deceaseds, :fiscal_code
    add_column :orders, :fiscal_code, :string, limit: 16
  end
end
