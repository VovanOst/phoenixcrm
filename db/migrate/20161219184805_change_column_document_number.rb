class ChangeColumnDocumentNumber < ActiveRecord::Migration
  def change
     change_column :documents, :document_number,  :string
  end
end
