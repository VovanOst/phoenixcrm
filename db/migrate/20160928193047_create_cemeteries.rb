class CreateCemeteries < ActiveRecord::Migration
  def change
    create_table :cemeteries do |t|
      t.string :name
      t.string :city
      t.string :province

      t.timestamps null: false
    end
  end
end
