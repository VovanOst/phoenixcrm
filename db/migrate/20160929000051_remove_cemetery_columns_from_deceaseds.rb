class RemoveCemeteryColumnsFromDeceaseds < ActiveRecord::Migration
  def change
    remove_column :deceaseds, :cemetery_1
    remove_column :deceaseds, :cemetery_2
  end
end
