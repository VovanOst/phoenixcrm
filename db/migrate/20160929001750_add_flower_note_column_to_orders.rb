class AddFlowerNoteColumnToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :flower_note, :string
  end
end
