class AddFiscalCodeToRelatives < ActiveRecord::Migration
  def change
    add_column :relatives, :fiscal_code, :string
  end
end
