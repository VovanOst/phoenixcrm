class AddDetailsToDocuments < ActiveRecord::Migration
  def change
    add_column :documents, :code_fiscale, :string
    add_column :documents, :issued_by_municipality, :string
  end
end
