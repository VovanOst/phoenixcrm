class AddIstrumentalToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :operation_3, :boolean, default: false
    add_column :orders, :operation_4, :boolean, default: false
    add_column :orders, :operation_5, :boolean, default: false
    add_column :orders, :operation_6, :boolean, default: false
    add_column :orders, :operation_7, :boolean, default: false
    add_column :orders, :note, :string
  end
end
