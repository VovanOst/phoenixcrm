class AddApplicantFaxmailToDeceaseds < ActiveRecord::Migration
  def change
    add_column :deceaseds, :applicant, :string
    add_column :deceaseds, :fax_mail, :string
  end
end
