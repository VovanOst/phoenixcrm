class RelativesController < ApplicationController
  before_action :set_orders, only: :index

  def index

  end

  private
    def set_orders
      if params.has_key?('marked')
        @contacts = Relative.card.marked
      elsif params.has_key?('ricent')
        @contacts = Relative.card.ordered
      end
    end
end
