class Relative < ActiveRecord::Base
  has_many :orders

  scope :card, -> { joins(:orders => :deceased).select('relatives.firstname as fn', 'relatives.lastname as ln', 'relatives.*', 'deceaseds.firstname', 'deceaseds.lastname', 'deceaseds.deathday') }
  scope :ordered, -> { order(:updated_at).reverse_order }
  scope :relationships, -> { uniq.pluck(:relationship) }

  scope :marked, -> { where('mark!=?', false) }
  scope :unmarked, -> { where('mark==?', false) }


  def name
    "#{self[:firstname]} #{self[:lastname]}"
  end
end
