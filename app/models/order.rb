class Order < ActiveRecord::Base
  belongs_to :deceased, autosave: true, dependent: :destroy, touch: true
  belongs_to :relative, autosave: true, dependent: :destroy, touch: true
  belongs_to :document, autosave: true, dependent: :destroy, touch: true
  belongs_to :cemetery, autosave: true, dependent: :destroy, touch: true
  belongs_to :crematorium, autosave: true, dependent: :destroy, touch: true
  belongs_to :epigraph, autosave: true, dependent: :destroy, touch: true
  has_many :flowers, dependent: :destroy
  has_many :assistants
  has_many :cars

  accepts_nested_attributes_for :cars, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :assistants, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :flowers, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :deceased, :update_only => true
  accepts_nested_attributes_for :relative, :update_only => true
  accepts_nested_attributes_for :document, :update_only => true
  accepts_nested_attributes_for :cemetery, :update_only => true
  accepts_nested_attributes_for :crematorium, :update_only => true
  accepts_nested_attributes_for :epigraph, :update_only => true

  validates :relative, presence: true
  validates :deceased, presence: true
  validates :document, presence: true
  validates :cemetery, presence: true
  validates :crematorium, presence: true
  validates :epigraph, presence: true

 # validates :fiscal_code, length: {maximum: 16}

  enum :docs_kind => [:check_list, :service_list, :flowers, :cremazione, :epigrafe, :mod_commis]
  enum :status => [:active, :archived]

  scope :full, -> { includes(:deceased, :relative, :flowers, :assistants, :cars, :epigraph, :document, :cemetery, :crematorium) }
  scope :part, -> { includes(:deceased, :relative) }

  scope :ordered, -> { order(:updated_at).reverse_order }
  scope :ordered_name, -> { part.order('deceaseds.lastname') }
  scope :ordered_relative, -> { part.order('relatives.lastname') }

  scope :active, -> { where('status=?', 0) }
  scope :archived, -> { where('status=?', 1) }

  def splitted_fiscal_code
    self[:fiscal_code].split(//)
  end

  def edited_at
    self[:updated_at].strftime(('%H:%M %d %b, %Y'))
  end

  def self.search(query)
    if query
      part.joins(:deceased, :relative).where('deceaseds.firstname like ? or deceaseds.lastname like ? or relatives.firstname like ? or relatives.lastname like ?', "%#{query}%", "%#{query}%", "%#{query}%", "%#{query}%")
    else
      part
    end
  end

end
