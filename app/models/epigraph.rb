class Epigraph < ActiveRecord::Base

  enum model: [:vuoto, :candle, :madonna, :madonna2, :red_roses, :white_lily]

  scope :dedicates, -> { uniq.pluck(:dedicated_to) }
  
  def date_vigil
    set_date(self[:date_vigil])
  end

  def time_vigil
    set_time(self[:time_vigil])
  end
  
private
    def set_date(date)
      if date.present?
        date.strftime('%d/%m/%Y')
      end
    end

    def set_time(time)
      if time.present?
        time.strftime('%H:%M')
      end
    end
end
