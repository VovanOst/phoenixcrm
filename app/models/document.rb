class Document < ActiveRecord::Base

  has_many :orders

  scope :full, -> { includes(:order) }

  def document_when_issued
    set_date(self[:document_when_issued])
  end

  private
    def set_date(date)
      if date.present?
        date.strftime('%d/%m/%Y')
      end
    end

    def set_time(time)
      if time.present?
        time.strftime('%H:%M')
      end
    end
end
