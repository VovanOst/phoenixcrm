class Crematorium < ActiveRecord::Base
  has_many :orders

 def Data
    set_date(self[:Data])
  end

before_validation on: [:create] do
    self.Data = Date.today
  end

   private
    def set_date(date)
      if date.present?
        date.strftime('%d/%m/%Y')
      end
    end

    def set_time(time)
      if time.present?
        time.strftime('%H:%M')
      end
    end

end
